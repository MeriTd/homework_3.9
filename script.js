/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */





/* 
Теоретичні питання
1. 
Відповідно до DOM кожен HTML-тег є об'єктом, HTML-теги є основою HTML-документа. Тобто це об'єктно орієнтована на HTML модель.

2. 
innerHTML повертає текстовий вміст елемента, включаючи всі пробіли та внутрішні теги HTML.
innerText повертає лише текстовий вміст елемента та всіх його дочірніх елементів, без прихованих текстових інтервалів і тегів CSS, за винятком елементів <script> і <style>.

3. 
Трохи застарілі способи: getElementById(), getElementsByClassName(), getElementsByName(), getElementsByTagName()
КРащі способи: querySelector(), querySelectorAll() 

4. 
HTMLCollection автоматично оновлюється при змінах у DOM, а nodeList статичний - показує те, що було при першому "виклику". 
NodeList може містити будь-який тип вузла, тоді як HTMLCollection містить лише елементи.

*/





/* 
Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 */


const featureCollection = document.getElementsByClassName("feature");
console.log(featureCollection);

const featureCollectionSecond = document.querySelectorAll(".feature");
console.log(featureCollectionSecond);

featureCollectionSecond.forEach((elem) => {
    elem.style.textAlign = 'center';
})




/* 
2. Змініть текст усіх елементів h2 на "Awesome feature".

 */

const h2paragraphs = document.querySelectorAll("h2");
console.log(h2paragraphs);

h2paragraphs.forEach((h2) => {
    h2.innerText = 'Awesome feature';
})





/*
3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
*/

const featureCollectionThird = document.querySelectorAll(".feature-title");
console.log(featureCollectionThird);

featureCollectionThird.forEach((text) => {
text.innerText = text.innerText + `!`;
})

